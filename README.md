# Desafio SM PLACES

## Tecnologias

Repositório com responsabilidade de disponibilizar uma stack com serviços que permitam trabalhar com o projeto.


## Tecnologias utilizadas

- Docker
- PHP 7.3 (php-fpm)
  - Laravel Framework
  - Eloquent ORM
- Nginx
- Postgres

## Estrutura de pastas

O projeto foi divido em 2 estruturas de pastas principais:

- [backend](./backend)
- database


## Serviços Disponíveis

Para reunir todos os módulos acima, foi criada uma stack Docker da aplicação, contendo os serviços abaixo:

- backend-service - Porta 8001
- db-service - host 0.0.0.0  / Porta  5401 / user usr_desafio / Password psw_desafio


## Como inicializar a Stack

O comando abaixo servirá para a criação das imagens utilizadas nos serviços mencionados acima.

```console
docker-compose up --build
```

## Testes
Para iniciar os testes
 
backend/vendor/bin/phpunit