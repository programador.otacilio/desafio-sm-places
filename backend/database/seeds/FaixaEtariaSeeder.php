<?php

use Illuminate\Database\Seeder;

class FaixaEtariaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tb_faixa_etaria')->insert( array(
            array('inicio'=>18,'fim' => 25),
            array('inicio'=>25,'fim' => 35),
            array('inicio'=>35,'fim' => 45),
            array('inicio'=>45,'fim' => 55),
            array('inicio'=>55,'fim' => null),
        ));
    }
}
