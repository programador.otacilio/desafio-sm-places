<?php

use Illuminate\Database\Seeder;

class TipoProvaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tb_tipo_prova')->delete();

        DB::table('tb_tipo_prova')->insert( array(
            array('co_tipo_prova'=>1,'ds_nome' => '3'),
            array('co_tipo_prova'=>2,'ds_nome' =>  '5'),
            array('co_tipo_prova'=>3,'ds_nome' =>  '10'),
            array('co_tipo_prova'=>4,'ds_nome' =>  '21'),
            array('co_tipo_prova'=>5,'ds_nome' =>  '42')
        ));
    }
}
