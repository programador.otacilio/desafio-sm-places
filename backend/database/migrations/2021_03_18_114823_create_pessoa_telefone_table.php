<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePessoaTelefoneTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_pessoa_telefone', function (Blueprint $table) {
            $table->increments('co_pessoa_telefone');
            $table->integer('co_pessoa');
            $table->integer('co_telefone');
            $table->foreign('co_pessoa')->references('co_pessoa')->on('tb_pessoa')->onDelete('cascade');
            $table->foreign('co_telefone')->references('co_telefone')->on('tb_telefone')->onDelete('cascade');
            //$table->integer('co_telefone');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_pessoa_telefone');
    }
}
