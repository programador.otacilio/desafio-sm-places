<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Services\Helpers;
use Illuminate\Validation\Rule;
use Core\Http\Requests\AFormRequest;
use App\Rules\CheckExistPessoa;
use App\Rules\UniqueTelefone;
use Request;


class TelefoneRequest extends AFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $sometimes = '';
        if(Request::route("co_telefone")!=null){
            $sometimes = 'sometimes'; 
        }
    	$rules = [
            'co_telefone'=>
            [
                'integer',
                $sometimes,
            ],
            'ds_telefone'=>[
                'required',
                'string',  
                new CheckExistPessoa(Request::route("co_pessoa")),     
                new UniqueTelefone(Request::route("co_telefone")),
            ],
            
        ];     
        
    	return $rules;
    }
    
    
    public function messages()
    {
    	return [
                'required' => 'O campo ":attribute" é obrigatório!',
    	];
    }

   
}
