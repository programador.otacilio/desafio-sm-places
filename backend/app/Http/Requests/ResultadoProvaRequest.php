<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Services\Helpers;
use Illuminate\Validation\Rule;
use Core\Http\Requests\AFormRequest;
use App\Rules\ExistInscricao;
use App\Rules\UniqueResultadoProva;
use App\Rules\CheckExistCorredor;



use Request;


class ResultadoProvaRequest extends AFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
    	$rules = [
            'co_corredor'=>
            [
                'required',
                'integer',
                new ExistInscricao(Request::route("id")),
                new UniqueResultadoProva(Request::route("id")),
                new CheckExistCorredor
            ],
            'th_hora_inicio'=>'required|string|size:8',
            'th_hora_fim'=>'required|string|size:8'
        ];
    	return $rules;
    }
    
    
    public function messages()
    {
    	return [
            'required' => 'O campo ":attribute" é obrigatório!', 
            'size'=>'O campo ":attribute" é Invalido!',

    	];
    }

   
}
