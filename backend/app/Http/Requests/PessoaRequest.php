<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Services\Helpers;
use Illuminate\Validation\Rule;
use Core\Http\Requests\AFormRequest;
use App\Rules\UniquePessoa;
use App\Rules\IdadePessoa;


use Request;


class PessoaRequest extends AFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
    	Helpers::validator_cpf();
    	
        $sometimes = '';
        if(Request::route("co_pessoa")!=null){
            $sometimes = 'sometimes'; 
        }

    	$rules = [
            'no_nome'=>[
                $sometimes,
                'required',
                'string',  
            ],
            'no_cpf' => [
                $sometimes,
                'required',
                'string',
                'validation_cpf',
                new UniquePessoa(Request::route("co_pessoa")),
            ],
            'dt_nascimento' => [
                $sometimes,
                'date_format:Y-m-d',
                'required',
                new IdadePessoa
            ],
        ];

     
        
    	return $rules;
    }
    
    
    public function messages()
    {
    	return [
                'validation_cpf'=>'CPF inválido',
                'required' => 'O campo ":attribute" é obrigatório!',
                'date_format'=>'O campo ":attribute" é inválido!'
               
    	];
    }

   
}
