<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class TelefoneResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return 
            [
                
                'telefone_id'=>isset($this->pivot->co_telefone) ? $this->pivot->co_telefone  : $this->co_telefone,
                //'nome' => isset($this->pivot->no_nome)? $this->pivot->no_nome : \App\Models\Pessoa::find($this->pivot->co_pessoa)->no_nome,
                'telefone'=>isset($this->pivot->pivotParent->ds_telefone) ? $this->pivot->pivotParent->ds_telefone  : $this->ds_telefone ,

            ];
        
    }
}
