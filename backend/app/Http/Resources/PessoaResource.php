<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PessoaResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
       $pessoa_service = app()->make(\App\Services\Pessoa::class);
       if(!isset($this->co_pessoa)){
           return [];
       }
        return 
            [
                'pessoa_id'=>$this->co_pessoa,
                'nome' => $this->no_nome,
                'cpf'=>$this->no_cpf,
                'nascimento'=>$this->dt_nascimento,
                'idade'=>$pessoa_service->calcularIdade($this->dt_nascimento).' Anos',
            ];
        
    }
}
