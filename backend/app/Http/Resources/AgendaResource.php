<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Services\Pessoa;
use App\Services\Telefone;
class AgendaResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        if(!isset($this->co_pessoa_telefone)){
            return [];
        }
        $pessoa = app()->make(Pessoa::class);
        $telefone = app()->make(Telefone::class);
        return 
            [
                'agenda_id'=>$this->co_pessoa_telefone,
                'pessoa'=> new PessoaResource($pessoa->buscar($this->co_pessoa)),
                'telefone'=>  TelefoneResource::collection($telefone->buscar($this->co_telefone)),
            ];
    }
}
