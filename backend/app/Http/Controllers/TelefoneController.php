<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Core\Http\Controllers\IApiResourceController;
use Illuminate\Http\Response;
use App\Services\Telefone as TelefoneService;
use App\Http\Requests\TelefoneRequest;
use App\Http\Resources\TelefoneResource;
class TelefoneController extends Controller implements IApiResourceController
{
    protected $service;

    public function __construct(TelefoneService $service)
    {
        $this->service = $service;
    }

    public function index($co_pessoa)
    {
        
        $result = count($this->service->buscarPorPessoa($co_pessoa)) ? 
        TelefoneResource::collection($this->service->buscarPorPessoa($co_pessoa)) : [];

        return $this->sendResponse(
            $result,
            __('responses.success.item'),
            Response::HTTP_OK
        );
    }

    public function store(TelefoneRequest $request)
    {
        
        return $this->sendResponse(
            $this->service->inserir($request),
            __('responses.success.create'),
            Response::HTTP_OK
        );
    }
    public function show($id)
    {   
        $result = count($this->service->buscar($id)) ? 
        TelefoneResource::collection($this->service->buscar($id)) : [];

        return $this->sendResponse(
            $result,
            __('responses.success.item'),
            Response::HTTP_OK
        );
    }
    public function update(TelefoneRequest $request, $id)
    {   
        
        return $this->sendResponse(
            $this->service->atualizarItem($request,$id),
            __('responses.success.update'),
            Response::HTTP_OK
        );
    }
    public function destroy($id)
    {
        return $this->sendResponse(
            $this->service->deletar($id),
            __('responses.success.destroy'),
            Response::HTTP_OK
        );
    }
}
