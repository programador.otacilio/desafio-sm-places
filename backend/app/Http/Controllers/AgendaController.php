<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Core\Http\Controllers\IApiResourceController;
use Illuminate\Http\Request;
use App\Http\Resources\AgendaResource;
use App\Services\PessoaTelefone as PessoaTelefoneService;
use Illuminate\Http\Response;

class AgendaController extends Controller implements IApiResourceController
{
    protected $service;
    
    public function __construct(PessoaTelefoneService $service)
    {
        $this->service = $service;
    }
    
    public function index()
    {       
        return $this->sendResponse(
            AgendaResource::collection($this->service->todos()),
            __('responses.success.item'),
            Response::HTTP_OK
        );

        
    }
    public function show($co_agenda)
    {      
        return $this->sendResponse(
            new AgendaResource($this->service->todos($co_agenda)),
            __('responses.success.item'),
            Response::HTTP_OK
        );
    }

    public function destroy($id)
    {
        return $this->sendResponse(
            $this->service->deletar($id),
            __('responses.success.destroy'),
            Response::HTTP_OK
        );
    }
    
}
