<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\Pivot;

class Pessoa extends Pivot
{
    protected $table = 'tb_pessoa';
    protected $primaryKey = 'co_pessoa';

    protected $fillable = [
        'no_nome',
        'no_cpf',
        'dt_nascimento',
    ];

    public function telefones()
    {
        
        return $this->belongsToMany(
            Telefone::class,
            'tb_pessoa_telefone',
            'co_pessoa',
            'co_telefone'
        );   
    }
}
