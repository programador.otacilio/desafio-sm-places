<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\Pivot;

class PessoaTelefone extends Pivot
{
    protected $table = 'tb_pessoa_telefone';
    protected $primaryKey = 'co_pessoa_telefone';
    public $timestamps = false;
    
    protected $fillable = [
        'co_pessoa',
        'co_telefone',
    ];

    
   
}
