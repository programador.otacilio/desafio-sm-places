<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\Pivot;

class Telefone extends Pivot
{
    protected $table = 'tb_telefone';
    protected $primaryKey = 'co_telefone';
    public $timestamps = false;
    
    protected $fillable = [
        'ds_telefone',
    ];

    
    public function telefones()
    {
        
        return $this->belongsToMany(
            Pessoa::class,
            'tb_pessoa_telefone',
            'co_telefone',
            'co_pessoa'
        );   
    }
}
