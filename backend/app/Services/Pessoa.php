<?php
namespace App\Services;
use App\Exceptions\ValidacaoCustomizadaException;
use Core\Services\AApiService;
use Illuminate\Http\Response;
use App\Models\Pessoa as PessoaModel;
use Carbon\Carbon;
use DB;

class Pessoa extends AApiService
{
    public function __construct(PessoaModel $model)
    {
        parent::__construct($model);
    }

    public function todos(){
        return $this->getModel()->all();
    }

    
    
    public function calcularIdade($dt_nascimento){
        if (strpos($dt_nascimento, '-')===false) {
            return 0;
        }

        $date = Carbon::createFromFormat('Y-m-d', $dt_nascimento);
        return $date->diffInYears(Carbon::now());
    }
    public function checkExistById($co_pessoa){
        $model =  $this->getModel()->where('co_pessoa',$co_pessoa);
        return $model->count() ? true : false;
    }
    public function checkExist($no_cpf,$co_corredor = null){
       $model =  $this->getModel()->where('no_cpf',Helpers::somente_numero($no_cpf));
        if($co_corredor!=null){
            $model = $model->where('co_pessoa','<>',$co_corredor);
        }
        return $model->count() ? true : false;
    }
    private function trata_dados($request){
        if(isset($request['no_cpf'])){
            $request['no_cpf'] = Helpers::somente_numero($request['no_cpf']); 
        }
        return $request;
    } 
    public function inserir($request)
    {
      
        try {
            DB::beginTransaction();
            $result = $this->getModel()->create($this->trata_dados($request->all()));
            DB::commit();
            return $result;
        }  catch (\HttpException $queryException) {
            DB::rollBack();
            throw $queryException;
        }
    }

    
    public function buscar($id)
    {
        
        return $this->getModel()->find($id);
        
    }
    public function atualizar($request,$id)
    {
        try {
            DB::beginTransaction();
            $model = $this->getModel()->find($id);
            if(!$model) {
                throw new ValidacaoCustomizadaException(
                    'Registro não encontrado',
                    Response::HTTP_NOT_FOUND
                );
            }
            $model->fill($request->all());
            $model->save();
            DB::commit();

            return $model;

        } catch (\Exception $exception) {
            DB::rollBack();
            throw $exception;
        }

       
    }
    public function deletar($id)
    {
        try {
            $model = $this->getModel()->find($id);
            if(!$model) {
                throw new ValidacaoCustomizadaException(
                    'Registro não encontrado',
                    Response::HTTP_NOT_FOUND
                );
            }
            $model->delete();
            return $model;
        }  catch (\Exception $exception) {
            DB::rollBack();
            throw $exception;
        }
    }

  
}
