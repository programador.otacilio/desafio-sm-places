<?php
namespace App\Services;
use App\Exceptions\ValidacaoCustomizadaException;
use Core\Services\AApiService;
use Core\Services\AApiResourceService;
use Illuminate\Http\Response;
use App\Models\Telefone as TelefoneModel;
use App\Models\Pessoa as PessoaModel;
use DB;

class Telefone extends   AApiResourceService
{   
    private $telefone_model;
    private $pessoa_model;
    
    public function __construct(TelefoneModel $telefone_model,PessoaModel $pessoa_model)
    {
        $this->telefone_model = $telefone_model;
        $this->pessoa_model = $pessoa_model;
        
        parent::__construct($telefone_model);
    }
    public function todos(){
        return $this->getModel()->all();
    }

    public function checkExist($ds_telefone,$co_telefone = null){
        $model =  $this->getModel()->where('ds_telefone',Helpers::somente_numero($ds_telefone));
         if($co_telefone!=null){
             $model = $model->where('co_telefone','<>',$co_telefone);
         }
         return $model->count() ? true : false;
     }
    public function inserir($request)
    {
        try {
            DB::beginTransaction();
            $result = $this->getModel()->create($request->all());
            $this->inserirTelefonePessoa($request,$result->co_telefone);
            DB::commit();
            return $result;
        }  catch (\HttpException $queryException) {
            DB::rollBack();
            throw $queryException;
        }
    }

    public function inserirTelefonePessoa($request,$co_telefone)
    {
        try {
            DB::beginTransaction();
            $dataInput = [
                'co_pessoa' =>$request->route('co_pessoa'),
                'co_telefone' =>$co_telefone,
            ];
            $result = app()->make(\App\Models\PessoaTelefone::class)->create($dataInput);
            DB::commit();
            return $result;
        }  catch (\HttpException $queryException) {
            DB::rollBack();
            throw $queryException;
        }
    }

    
    public function buscarPorPessoa($co_pessoa)
    {
        $result_model  = app()->make(\App\Models\Pessoa::class)->find($co_pessoa);
        if($result_model){
            return $result_model->telefones()->get();
        }
        return [];   
    }
    public function buscar($id)
    {
        $result_model  = app()->make(\App\Models\Telefone::class)->find($id);
        if($result_model){
            return $result_model->telefones()->get();
        }
        return [];   
    }
    public function atualizarItem($request,$id)
    {
        try {
            DB::beginTransaction();
            $model = $this->getModel()->find($id);
            if(!$model) {
                throw new ValidacaoCustomizadaException(
                    'Registro não encontrado',
                    Response::HTTP_NOT_FOUND
                );
            }

            
            $model->fill($request->all());
            $model->save();
            DB::commit();

            return $model;

        } catch (\Exception $exception) {
            DB::rollBack();
            throw $exception;
        }

       
    }
    public function deletar($id)
    {
        try {
            $model = $this->getModel()->find($id);
            if(!$model) {
                throw new ValidacaoCustomizadaException(
                    'Registro não encontrado',
                    Response::HTTP_NOT_FOUND
                );
            }
            $model->delete();
            return $model;
        }  catch (\Exception $exception) {
            DB::rollBack();
            throw $exception;
        }
    }
}
