<?php
namespace App\Services;
use App\Exceptions\ValidacaoCustomizadaException;
use Core\Services\AApiService;
use Illuminate\Http\Response;
use App\Models\PessoaTelefone as PessoaTelefoneModel;
use Carbon\Carbon;
use DB;

class PessoaTelefone extends AApiService
{
    
    public function __construct(PessoaTelefoneModel $model)
    {
        parent::__construct($model);
    }
    public function todos($co_pessoa_telefone=null){

        if($co_pessoa_telefone==null){
            return app()->make(\App\Models\PessoaTelefone::class)->all();
        }
        return app()->make(\App\Models\PessoaTelefone::class)->find($co_pessoa_telefone);
        
    }

    public function deletar($id)
    {
        try {
            $model = $this->getModel()->find($id);
            if(!$model) {
                throw new ValidacaoCustomizadaException(
                    'Registro não encontrado',
                    Response::HTTP_NOT_FOUND
                );
            }
            $model->delete();
            return $model;
        }  catch (\Exception $exception) {
            DB::rollBack();
            throw $exception;
        }
    }
}
