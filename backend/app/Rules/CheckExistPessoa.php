<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use Request;
class CheckExistPessoa implements Rule
{
   /**
     * Create a new rule instance.
     *
     * @return void
     */
    private $co_pessoa;
    public function __construct($co_pessoa)
    {
        $this->co_pessoa = $co_pessoa;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return app()->make(\App\Services\Pessoa::class)->buscar($this->co_pessoa);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Pessoa não encontrada.';
    }
}
