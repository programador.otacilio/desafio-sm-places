<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class IdadePessoa implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */

    private $idadeMinima  = 12;
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $idade =  app()->make(\App\Services\Pessoa::class)->calcularIdade($value);
        if($idade>=$this->idadeMinima){
            return true;
        }

        return false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'A idade da pessoa deve ser maior ou igual '.$this->idadeMinima;
    }
}
