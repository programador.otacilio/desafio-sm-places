<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
class UniquePessoa implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    private $id;
    public function __construct($id = null)
    {
        $this->id = $id;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return  !app()->make(\App\Services\Pessoa::class)->checkExist($value,$this->id);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'CPF já Cadastrado';
    }
}
