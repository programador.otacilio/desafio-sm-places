<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(array('prefix' => 'api'), function () {    
    Route::group(array('prefix' => 'pessoa'), function () {
        Route::get('', 'PessoaController@index');
        Route::post('', 'PessoaController@store');
        Route::get('{co_pessoa}', 'PessoaController@show');
        Route::put('{co_pessoa}', 'PessoaController@update');
        Route::delete('{co_pessoa}', 'PessoaController@destroy');    

        Route::post('{co_pessoa}/telefone', 'TelefoneController@store');
        Route::get('{co_pessoa}/telefone', 'TelefoneController@index');
    });

    Route::group(array('prefix' => 'telefone'), function () {
        Route::get('{id}', 'TelefoneController@show');
        Route::put('{id}', 'TelefoneController@update');
        Route::delete('{co_telefone}', 'TelefoneController@destroy');    
    });

    Route::group(array('prefix' => 'agenda'), function () {
        Route::get('', 'AgendaController@index');
        Route::get('{id}', 'AgendaController@show');
        Route::delete('{id}', 'AgendaController@destroy');    
    });
    
  
    
});