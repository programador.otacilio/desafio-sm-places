<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class PessoaTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */

     
    public function testIdade()
    {
        $idade = app()->make(\App\Services\Pessoa::class)->calcularIdade('1989-12-28');
        $this->assertEquals(31,$idade);
    }

    public function testCheckIfPessoaColumsIsCorrect()
    {
        $pessoa = app()->make(\App\Models\Pessoa::class);

        $expected = [
            'no_nome',
            'no_cpf',
            'dt_nascimento',
        ];

        $arrayCompared = array_diff($expected,$pessoa->getFillable());
        $this->assertEquals(0,count($arrayCompared));
        
    }
}
